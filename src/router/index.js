import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'

const Dashboard = () => import(/* webpackChunkName: "dashboards" */'../views/dashboard/dashboard.vue');
const Category = () => import(/* webpackChunkName: "category" */'../views/pages/category.vue');
const Product = () => import(/* webpackChunkName: "product" */'../views/pages/product.vue');
const Cart = () => import(/* webpackChunkName: "cart" */'../views/pages/cart.vue');
const Order = () => import(/* webpackChunkName: "order" */'../views/pages/order.vue');
const Payment = () => import(/* webpackChunkName: "payment" */'../views/pages/payment.vue');
const Login = () => import(/* webpackChunkName: "login" */'../views/pages/login.vue');
const ForgetPassword = () => import(/* webpackChunkName: "forget" */'../views/pages/forgetPassword.vue');
const Notif = () => import(/* webpackChunkName: "notif" */'../views/pages/notif.vue');
const Account = () => import(/* webpackChunkName: "register" */'../views/pages/account.vue');
const UserDataForms = () => import(/* webpackChunkName: "userData" */'../views/pages/UserDataForms.vue');
const UserAddressForms = () => import(/* webpackChunkName: "userAddress" */'../views/pages/UserAddressForms.vue');
const OrderSuccess = () => import(/* webpackChunkName: "orderSuccess" */'../views/pages/OrderSuccess.vue');
const successValidate = () => import(/* webpackChunkName: "successValidate" */'../views/pages/successValidate.vue');
const editPassword = () => import(/* webpackChunkName: "editPassword" */'../views/pages/editPassword.vue');


Vue.use(VueRouter)

const routes = [  
  {
    path: '/',
    name: 'dashboard',
    component: Dashboard

  },
  {
    path: '/collection/:category',
    name: 'category-product',
    component: Category
  },
  {
    path: '/product/:product_id',
    name: 'product',
    component: Product
  },
  {
    path: '/cart',
    name: 'cart',
    component: Cart,
    beforeEnter(to, from, next) {
      if (!store.getters.isAuthenticated(Date.now())) {
        next('/auth')
      } else {
        next()
      }
    },
  },
  {
    path: '/order',
    name: 'order',
    component: Order,    
    beforeEnter(to, from, next) {
      if (!store.getters.isAuthenticated(Date.now())) {
        next('/auth')
      } else {
        next()
      }
    },
  },
  {
    path: '/order/payment',
    name: 'payment',
    component: Payment,
    props:true,
    beforeEnter(to, from, next) {
      if (!store.getters.isAuthenticated(Date.now())) {
        next('/auth')
      } else {
        next()
      }
    },
  },
  {
    path: '/auth',
    name: 'login',
    component: Login
  },
  {
    path: '/auth/forget',
    name: 'forget',
    component: ForgetPassword
  },
  {
    path: '/auth/info',    
    name: 'notif',
    component: Notif,
    props:true
  },
  {
    path: '/auth/validate/:token',    
    name: 'validate-email',
    component: successValidate    
  },
  {
    path: '/auth/register',
    name: 'register',
    component: UserDataForms
  },
  {
    path: '/auth/account/:token',
    name: 'change-password',
    component: editPassword
  },
  {
    path: '/account',
    name: 'account',
    component: Account,
    beforeEnter(to, from, next) {
      if (!store.getters.isAuthenticated(Date.now())) {
        next('/auth')
      } else {
        next()
      }
    },
  },
  {
    path: '/account/edit',
    name: 'account-edit',
    component: UserDataForms,
    props:true,
    beforeEnter(to, from, next) {
      if (!store.getters.isAuthenticated(Date.now())) {
        next('/auth')
      } else {
        next()
      }
    },
  },
  {
    path: '/account/address/edit',
    name: 'address-edit',
    component: UserAddressForms,
    props:true,
    beforeEnter(to, from, next) {
      if (!store.getters.isAuthenticated(Date.now())) {
        next('/auth')
      } else {
        next()
      }
    },
  },
  {
    path: '/order/success',
    name: 'order-notif',
    component: OrderSuccess,
    beforeEnter(to, from, next) {
      if (!store.getters.isAuthenticated(Date.now())) {
        next('/auth')
      } else {
        next()
      }
    },
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

