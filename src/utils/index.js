import store from '../store';

export function isValidJwt (jwt, now) {    
  if (!jwt || jwt.split('.').length < 3) {        
    return false;
  }
  const data = JSON.parse(atob(jwt.split('.')[1]))
  const exp = new Date(data.exp * 1000)

  if (now >= exp){        
    store.commit('reset'); 
  }
  
  return now < exp
}
