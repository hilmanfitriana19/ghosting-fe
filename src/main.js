import Vue from 'vue'
import App from './App.vue'
import store from './store'
import VeeValidate from 'vee-validate'
import VueSweetalert2 from 'vue-sweetalert2'
import router from './router'
import 'semantic-ui-css/semantic.min.css'
import 'sweetalert2/dist/sweetalert2.all.min.js'
import globaljs from '../src/assets/js/global'

import {APIServices} from './api/ApiServices';
const apiServices = new APIServices();
Object.defineProperty(Vue.prototype, '$api', { value: apiServices });
Vue.config.productionTip = false;

Vue.mixin(globaljs)

Vue.use(VueSweetalert2)
Vue.use(VeeValidate)

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
