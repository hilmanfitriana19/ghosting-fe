// store/index.js

// imports of AJAX functions will go here
import { isValidJwt } from '../utils'
import createPersistedState from 'vuex-persistedstate'

import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

function initialState() {
    return {        
        jwt:'',
        product_count:null,
        name:'',
        imgPath:[]
    }
}

const state = {    
        jwt:'',
        product_count:null,
        name:'',
        imgPath:[]
}

const actions = {
    // asynchronous operations
    storeLoginData(context, payload) {
        context.commit('setJwtToken', payload)
        context.commit('setName',payload)
    },
    storeImageListPath(context, data) {
        context.commit('setImagePath', data)
    },
    storeCartCount(context, data) {
        context.commit('setCartCount',data)
    },
}

const mutations = {
    // isolated data mutations    
    setImagePath(state,data){
        state.imgPath= data;        
    },
    setJwtToken(state, payload) {
        localStorage.token = payload.token
        state.jwt = payload.token
    },     
    setName(state,payload)   {
        localStorage.name = payload.username
        state.name = payload.username
    },
    reset(state) {
        const s = initialState()
        Object.keys(s).forEach(key => {
            if(key!='imgPath')
                state[key] = s[key]
        })
    },
    setCartCount(state,data){
        state.product_count=data
    }
}

const getters = {    
    // reusable data accessors
    isAuthenticated(state) {
        return function (now) {            
            return isValidJwt(state.jwt, now)
        };
    },    
}

const store = new Vuex.Store({
    state,
    actions,
    mutations,
    getters,
    plugins: [
        createPersistedState()
    ]
})
export default store
