export default{
    methods: {
        handleError:function(error){
            if(error.response==null){
                this.$swal.fire({                  
                    icon: 'error',
                    title: 'Connect to Database Error',                            
                });
            }
        },
        sleep:function(ms) {
            return new Promise((resolve) => {
                setTimeout(resolve, ms);
            });
        },
        validateForm:function(){
            this.$swal.fire({
                icon: "error",            
                title: "Please Fill Required Field",                
                focusConfirm: true
              });   
        }
    },
}