import axios from 'axios';
import store from '../store';

// const API_URL = 'https://ghosting-forlife.com/api';
const API_URL = 'http://localhost:5001/api';

export class APIServices {
    constructor() { }
    
    login(userData) {
        return axios.post(`${API_URL}/auth/`, userData);
    }

    logout() {
        const url = `${API_URL}/auth/logout`;        
        const headers = {"api-token": store.state.jwt};
        return axios.post(url,null,{ headers: headers }).then(response => response);}
    register(user) {
        return axios.post(`${API_URL}/auth/register`, user);
    }    
    verify_email(token){
        const url = `${API_URL}/auth/verify/${token}`;        
        return axios.get(url);
    }
    forget(email){
        const url = `${API_URL}/auth/forget`;        
        return axios.post(url, {'email':email});
    }
    change_password(token,password){
        const url = `${API_URL}/auth/forget/${token}`;
        return axios.post(url,{'password':password}).then(response => response);
    }
    getUser() {
        const url = `${API_URL}/auth/`;
        const headers = {"api-token": store.state.jwt};
        return axios.get(url,{ headers: headers }).then(response => response.data);
    }
    editUser(data) {
        const url = `${API_URL}/auth/`;
        const headers = {"api-token": store.state.jwt};
        return axios.put(url,data,{ headers: headers }).then(response => response.data);
    }
    getUserCart() {
        const url = `${API_URL}/cart/`;
        const headers = {"api-token": store.state.jwt};
        return axios.get(url,{ headers: headers }).then(response => response.data);
    }
    getUserCartCount() {
        const url = `${API_URL}/cart/count`;
        const headers = {"api-token": store.state.jwt};
        return axios.get(url,{ headers: headers }).then(response => response.data);
    }

    addProductToCart(data){
        const url = `${API_URL}/cart/product`;
        const headers = {"api-token": store.state.jwt};
        return axios.post(url, data, { headers: headers });
    }    
    editProductToCart(data){
        const url = `${API_URL}/cart/product`;
        const headers = {"api-token": store.state.jwt};
        return axios.put(url, data, { headers: headers });
    }    
    
    removeProductFromCart(data) {
        const url = `${API_URL}/cart/product`;
        const headers = {"api-token": store.state.jwt};
        return axios.delete(url,{data, headers: headers });
    }

    getListCategory(){
        const url = `${API_URL}/category/`;        
        return axios.get(url).then(response => response);
    }
    getListCategoryFull(){
        const url = `${API_URL}/category/detail`;
        return axios.get(url).then(response => response);
    }
    
    createCategory(Category) {
        const url = `${API_URL}/category/`;
        const headers = {"api-token": store.state.jwt};
        return axios.post(url, Category,{ headers: headers });
    }
            
    deleteCategory(pk) {
        const url = `${API_URL}/category/${pk}`;
        const headers = {"api-token": store.state.jwt};
        return axios.delete(url,{ headers: headers });
    }    
    
    updateCategory(pk,category) {
        const url = `${API_URL}/category/${pk}`;
        const headers = {"api-token": store.state.jwt};
        return axios.put(url, category,{ headers: headers });
    }

    getListProductCategory(category){
        const url = `${API_URL}/product/`;        
        return axios.get(url,{'category':category}).then(response => response);
    }
    getDetailProduct(product_id){
        const url = `${API_URL}/product/detail/${product_id}`;        
        return axios.get(url).then(response => response);
    }
    
    addProduct(product) {
        const url = `${API_URL}/product/`;
        const headers = {"api-token": store.state.jwt};
        return axios.post(url, product,{ headers: headers }).then(response => response);
    }

    editProduct(product_id, data) {
        const url = `${API_URL}/product/${product_id}`;
        const headers = {"api-token": store.state.jwt};
        return axios.put(url, data,{ headers: headers });
    }
        
    addVariant(variant) {
        const url = `${API_URL}/product/variant`;
        const headers = { "api-token": store.state.jwt };
        return axios.post(url, variant,{ headers: headers });
    }
    
    updateCodex(variant) {
        const url = `${API_URL}/product/variant`;
        const headers = { "api-token": store.state.jwt };
        return axios.put(url, variant,{ headers: headers });
    }
        
    getVariant() {
        const url = `${API_URL}/product/variant`;        
        return axios.get(url).then(response => response);
    }
        
    createProductDetail(detail) {
        const url = `${API_URL}/product/detail`;
        const headers = { "api-token": store.state.jwt };
        return axios.post(url, detail,{ headers: headers });
    }

    updateProductDetail(detail) {
        const url = `${API_URL}/product/detail`;
        const headers = {"api-token": store.state.jwt};
        return axios.put(url, detail,{ headers: headers });
    }

    deleteProductDetail(data) {
        const url = `${API_URL}/product/detail`;
        const headers = {"api-token": store.state.jwt};
        return axios.delete(url,data,{ headers: headers });
    }

    deleteVariant(data) {
        const url = `${API_URL}/product/variant`;
        const headers = {"api-token": store.state.jwt};
        return axios.delete(url,data,{ headers: headers });
    }

    deleteProduct(data) {
        const url = `${API_URL}/product/`;
        const headers = {"api-token": store.state.jwt};
        return axios.delete(url,data,{ headers: headers });
    }
    
    createTransaction(data){
        const url = `${API_URL}/transaction/`;
        const headers = { "api-token": store.state.jwt };
        return axios.post(url, data,{ headers: headers });
    }
    getListTransaction(){
        const url = `${API_URL}/transaction/`;
        const headers = { "api-token": store.state.jwt };
        return axios.get(url,{ headers: headers });
    }
    
    getUserAddress() {
        const url = `${API_URL}/user/address/`;
        const headers = {"api-token": store.state.jwt};
        return axios.get(url,{ headers: headers }).then(response => response.data);
    }

    addUserAddress(data) {
        const url = `${API_URL}/user/address/`;
        const headers = {"api-token": store.state.jwt};
        return axios.post(url,data,{ headers: headers });
    }
    getOneUserAddress(id) {
        const url = `${API_URL}/user/address/${id}`;
        const headers = {"api-token": store.state.jwt};
        return axios.get(url,{ headers: headers }).then(response => response.data);
    }
    editUserAddress(data){
        const url = `${API_URL}/user/address/`;
        const headers = {"api-token": store.state.jwt};
        return axios.put(url, data,{ headers: headers });
    }
    deleteUserAddress(id){
        const url = `${API_URL}/user/address/${id}`;
        const headers = {"api-token": store.state.jwt};
        return axios.delete(url,{ headers: headers });
    }

}
