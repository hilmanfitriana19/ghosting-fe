const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
    chainWebpack: config => {
        config.plugins.delete('prefetch');
    },
    configureWebpack: {
        plugins: [new BundleAnalyzerPlugin()],
        optimization: {
            splitChunks: {
                minSize: 10000,
                maxSize: 25000,
            },
        }
    }
}